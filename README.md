# Plotly

Start the web app in `reload` mode:

```sh
$ flask run --reload -h 0.0.0.0
* Environment: production
...
* Running on all addresses (0.0.0.0)
* Running on http://127.0.0.1:5000/ (Press CTRL+C to quit)
```

Open a terminal and get IP addr:

```
$ ip --brief addr
```

Fire up a web browser and navigate to http://&lt;ip&gt;:5000

## Plotly sample datasets and collab file:

You can find some datasets in `data` folder. 

The source code and references from all datasets can be found in:

https://colab.research.google.com/drive/1sZao6cBj5JiiZ9NBJAeC2_qzwCidjSDS?usp=sharing

https://docs.google.com/document/d/1u90XI23NT6qDCb28IIsuUcKAUt_8XNgYe_lKSiT2Zfk/edit?usp=sharing

TODO. Actualitzar site.